`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/20 19:04:54
// Design Name: 
// Module Name: if_stage_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module if_stage_tb();
    reg cpu_clk_50M, cpu_rst_n;
    reg ice;
    reg [31:0] pc;
    wire [31:0] iaddr;
    
    if_stage ise(
        .cpu_clk_50M(cpu_clk_50M),
        .cpu_rst_n(cpu_rst_n),
        .ice(ice),
        .pc(pc),
        .iaddr(iaddr)
    );
    
    initial begin
        cpu_clk_50M <= 0;
        cpu_rst_n <= 0;
        #100 cpu_rst_n <= 1;
    end
    
    initial begin
        @
    end
    
    always #10  cpu_clk_50M = ~cpu_clk_50M;
    
endmodule
