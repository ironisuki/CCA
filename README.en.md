# 计算机组成与体系结构

#### Description
实验任务一：基于MIPS32的指令集仿真器的设计与实现
实验任务二：基于MIPS32的典型流水线处理器的设计与实现
实验任务三：流水线处理器的冒险消除与异常处理
实验任务四：高速缓冲存储器Cache的设计与实现

#### 进度
利用定向前推解决数据相关
解决控制相关
实现J,JR,JAL,BEQ,BNE指令及测试
实现BGEZ,BGTZ,BLEZ,BLTZ,BLTZAL,BGEZAL,JALR及测试
完成divu指令和div指令
解决加载相关问题
完成mtc0，mtc0，syscall，break，eret指令
完成溢出异常
完成（取指）地址错误异常(not-test)，保留指令异常(not-test)，（访存）加载地址错误异常(tested)，（写回）存储地址异常(tested)

stall.S
exc_test.S
jump.S
jump1.S