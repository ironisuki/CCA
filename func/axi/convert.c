#include <stdio.h>
#include <stdlib.h>
#include <elf.h>
#include <assert.h>
#define DATA_BASE (0x10000)

int main(int argc, char* argv[])
{
	FILE *in;
	FILE *out;

	uint8_t *file_bin_name = malloc(80);
	uint8_t *file_data_name = malloc(80);

	int i,j,k;
	unsigned char mem[32];

	strcat(file_bin_name, argv[1]);  
	strcat(file_data_name, argv[2]);

    in = fopen(file_bin_name, "rb");
    out = fopen("inst_rom.coe","w");

	fprintf(out, "memory_initialization_radix = 16;\n");
	fprintf(out, "memory_initialization_vector =\n");

    int writeByte = 0;

    while (!feof(in))
    {
        if(fread(mem,1,4,in)!=4) {
	        fprintf(out, "%02x%02x%02x%02x\n", mem[0], mem[1],	mem[2], mem[3]);
            printf(">>>>>> WirteByte = %d\n",writeByte);
            break;
         }
	    fprintf(out, "%02x%02x%02x%02x\n", mem[0], mem[1], mem[2],mem[3]);
        writeByte += 4;
    }
    while(writeByte != DATA_BASE - 4 ){
        fprintf(out, "%02x%02x%02x%02x\n", 0, 0, 0, 0);
        writeByte += 4;
    }
    fclose(in);
    in = fopen(file_data_name, "rb");
    printf("writeLne = %d \n", writeByte);

    while (!feof(in))
    {
        if(fread(mem,1,4,in)!=4) {
	        fprintf(out, "%02x%02x%02x%02x\n", mem[0], mem[1],	mem[2], mem[3]);
		break;
	     }
	    fprintf(out, "%02x%02x%02x%02x\n", mem[0], mem[1], mem[2],mem[3]);
    }
	fclose(in);
	fclose(out);

    return 0;
}
