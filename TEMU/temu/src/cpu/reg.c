#include "temu.h"
#include <stdlib.h>

CPU_state cpu;
CP0_state cp0;

const char *regfile[] = {"$zero", "$at", "$v0", "v1", "$a0", "$a1", "$a2", "$a3", "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$t8", "$t9", "$k0", "$k1", "$gp", "$sp", "$fp", "$ra"};
const char *cp0file[] = {"$index", "$random", "$entrylo0", "$entrylo1", "$context", "$pagemask", "$wired", "reserved_7", "$badvaddr", "$count", "$entryhi", "$compare", "$status", "$cause", "$epc", "$prid", "$config", "$lladdr", "$watchlo", "$watchhi", \
"reserved_20", "reserved_21", "reserved_22", "$debug", "$depc", "reserved_25", "$errctl", "reserved_27", "$taglo_Datalo", "reserved_29", "$errorepc", "$desave"};

void display_reg() {
        int i;
        for(i = 0; i < 32; i ++) {
                printf("%s\t\t0x%08x\t\t%d\n", regfile[i], cpu.gpr[i]._32, cpu.gpr[i]._32);
        }

        printf("%s\t\t0x%08x\t\t%d\n", "$pc", cpu.pc, cpu.pc);
        printf("%s\t\t0x%08x\t\t%d\n", "$lo", cpu.lo, cpu.lo);
        printf("%s\t\t0x%08x\t\t%d\n", "$hi", cpu.hi, cpu.hi);
        
        printf("cp0 reg\n\n");
        for(i = 0; i < 32; i++){
                printf("%s\t\t0x%08x\t\t%d\n", cp0file[i], cp0.gpr[i]._32, cp0.gpr[i]._32);
        }

}

