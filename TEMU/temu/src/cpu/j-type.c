#include "helper.h"
#include "monitor.h"
#include "reg.h"

extern uint32_t instr;
extern char assembly[80];

static void decode_j_type(uint32_t instr){
    op_dest->type = OP_TYPE_JUMP;
    op_dest->instr_index = instr & INDEX_MASK;
    op_dest->val = op_dest->instr_index;
}

make_helper(j){
    decode_j_type(instr);
    uint32_t offset = ((cpu.pc >> 28) << 28) + (op_dest->val << 2) - 4; 
    cpu.pc = (int)offset;
    sprintf(assembly, "j   0x%04x", op_dest->instr_index);
}

make_helper(jal){
    decode_j_type(instr);
    cpu.gpr[31]._32 = cpu.pc + 8;
    uint32_t offset = ((cpu.pc >> 28) << 28) + (op_dest->val << 2) - 4; 
    cpu.pc = (int)offset;
    sprintf(assembly, "jal   0x%04x", op_dest->instr_index);
}