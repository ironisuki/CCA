#include "monitor.h"
#include "expr.h"
#include "temu.h"
#include "watchpoint.h"

#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <memory/memory.h>

void cpu_exec(uint32_t);

void display_reg();

/* We use the `readline' library to provide more flexibility to read from stdin. */
char* rl_gets() {
	static char *line_read = NULL;

	if (line_read) {
		free(line_read);
		line_read = NULL;
	}

	line_read = readline("(temu) ");

	if (line_read && *line_read) {
		add_history(line_read);
	}

	return line_read;
}

static int cmd_c(char *args) {
	cpu_exec(-1);
	return 0;
}

static int cmd_q(char *args) {
	return -1;
}

static int cmd_help(char *args);

static int cmd_si(char *args);

static int cmd_info(char *args);

static int cmd_x(char *args);

static int cmd_p(char *args);

static int cmd_w(char *args);

static int cmd_d(char *args);

static struct {
	char *name;
	char *description;
	int (*handler) (char *);
} cmd_table [] = {
	{ "help", "Display informations about all supported commands", cmd_help },
	{ "c", "Continue the execution of the program", cmd_c },
	{ "q", "Exit TEMU", cmd_q },
	{ "si", "The program pauses after executing N instructions in a single step. When n is not given, it defaults to 1", cmd_si},
	{ "info", "Print register and monitor point information",  cmd_info},
	{ "x", "Scan memory",  cmd_x},
	{ "p", "Expression evaluation", cmd_p},
	{ "w", "When the value of expression expr changes, program execution is suspended.", cmd_w},
	{ "d", "Delete the watchpoint", cmd_d}
	/* TODO: Add more commands */

};

#define NR_CMD (sizeof(cmd_table) / sizeof(cmd_table[0]))

static int cmd_help(char *args) {
	/* extract the first argument */
	char *arg = strtok(NULL, " ");
	int i;

	if(arg == NULL) {
		/* no argument given */
		for(i = 0; i < NR_CMD; i ++) {
			printf("%s - %s\n", cmd_table[i].name, cmd_table[i].description);
		}
	}
	else {
		for(i = 0; i < NR_CMD; i ++) {
			if(strcmp(arg, cmd_table[i].name) == 0) {
				printf("%s - %s\n", cmd_table[i].name, cmd_table[i].description);
				return 0;
			}
		}
		printf("Unknown command '%s'\n", arg);
	}
	return 0;
}

static int cmd_si(char *args){
	char *arg = strtok(NULL, " ");
	if(arg == NULL){
		//defalut to 1
		cpu_exec(1);
	}else{
		int n = atoi(arg);
		cpu_exec(n);
	}
	return 0;
}

static int cmd_info(char *args){
	char *arg = strtok(NULL, " ");
	if(!strcmp(arg, "r")){
		display_reg();
	}else if(!strcmp(arg, "w")){
		display_watchpoint();
	}else{
		printf("Invalid expression\n");
	}
	return 0;
}

static int cmd_x(char *args){
	char *arg = strtok(NULL, " ");
	int n;
	sscanf(arg, "%d", &n);

	arg = arg + strlen(arg) + 1;

	bool success;
	uint32_t address = expr(arg, &success);
	address = address & 0x1fffffff;
	int i;
	for(i = 0; i < n; i++){
		printf("0x%x\n", mem_read(address + i * 4,  4));
	}
	return 0;
}

static int cmd_p(char *args){
	bool success;
	uint32_t ans = expr(args, &success);
	if(success){
		printf("dec: %d\n", ans);
		printf("hex: 0x%x\n", ans);
	}else{
		panic("Expression evaluation failed\n");
	}
	return 0;
}

static int cmd_w(char *args){
	WP *f;
	f = new_wp();
	bool success;
	printf("Watchpoint %d\t%s\n", f->NO, args);
	f->val = expr(args, &success);
	strcpy(f->expr, args);
	printf("Watchpoint value %d\t\n", f->val);
	return 0;
}

static int cmd_d(char *args){
	int num;
	sscanf(args, "%d", &num);
	printf("Delete watchpoint %d\n",  num);
	delete_wp(num);
	return 0;
}

void ui_mainloop() {
	while(1) {
		char *str = rl_gets();
		char *str_end = str + strlen(str);

		/* extract the first token as the command */
		char *cmd = strtok(str, " ");
		if(cmd == NULL) { continue; }

		/* treat the remaining string as the arguments,
		 * which may need further parsing
		 */
		char *args = cmd + strlen(cmd) + 1;
		if(args >= str_end) {
			args = NULL;
		}

		int i;
		for(i = 0; i < NR_CMD; i ++) {
			if(strcmp(cmd, cmd_table[i].name) == 0) {
				if(cmd_table[i].handler(args) < 0) { return; }
				break;
			}
		}

		if(i == NR_CMD) { printf("Unknown command '%s'\n", cmd); }
	}
}
