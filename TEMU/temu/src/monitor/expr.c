#include "temu.h"

/* We use the POSIX regex functions to process regular expressions.
 * Type 'man regex' for more information about POSIX regex functions.
 */
#include <memory/memory.h>
#include <sys/types.h>
#include <regex.h>

enum {
	NOTYPE = 256, EQ, DEC, HEX, REG, DEREF, MINUS, AND, OR, NEQ
	
	/* TODO: Add more token types */

};

static struct rule {
	char *regex;
	int token_type;
	int priority;
} rules[] = {

	/* TODO: Add more rules.
	 * Pay attention to the precedence level of different rules.
	 */
	{"\\b[0-9]+\\b",DEC,0},				// number
	{"\\b0[xX][0-9a-fA-F]+\\b",HEX,0},		// 16 number
	{"\\$[a-zA-Z]+",REG,0},				// register
	{" +",	NOTYPE, 0},				// spaces
	{"\\|\\|",OR,1},					//or
	{"&&",AND,2},						//and
	{"==", EQ, 3},						// equal
	{"\\+", '+', 4},					// plus
	{"-", '-', 4},						//minus
	{"\\*", '*', 5},				//mul
	{"/", '/', 5},						//div
	{"!",'!',6},						// not
	{"\\(", '(', 7},					// (
	{"\\)", ')', 7}						// )
};

#define NR_REGEX (sizeof(rules) / sizeof(rules[0]) )

static regex_t re[NR_REGEX];

/* Rules are used for many times.
 * Therefore we compile them only once before any usage.
 */
void init_regex() {
	int i;
	char error_msg[128];
	int ret;

	for(i = 0; i < NR_REGEX; i ++) {
		ret = regcomp(&re[i], rules[i].regex, REG_EXTENDED);
		if(ret != 0) {
			regerror(ret, &re[i], error_msg, 128);
			Assert(ret == 0, "regex compilation failed: %s\n%s", error_msg, rules[i].regex);
		}
	}
}

typedef struct token {
	int type;
	char str[32];
	int priority;
} Token;

Token tokens[32];
int nr_token;

static bool make_token(char *e) {
	// printf("%s\n", e);
	int position = 0;
	int i;
	regmatch_t pmatch;
	
	nr_token = 0;

	while(e[position] != '\0') {
		/* Try all rules one by one. */
		for(i = 0; i < NR_REGEX; i ++) {
			if(regexec(&re[i], e + position, 1, &pmatch, 0) == 0 && pmatch.rm_so == 0) {
				char *substr_start = e + position;
				int substr_len = pmatch.rm_eo;
				//Log("match rules[%d] = \"%s\" at position %d with len %d: %.*s", i, rules[i].regex, position, substr_len, substr_len, substr_start);
				position += substr_len;

				/* TODO: Now a new token is recognized with rules[i]. Add codes
				 * to record the token in the array `tokens'. For certain types
				 * of tokens, some extra actions should be performed.
				 */

				switch(rules[i].token_type) {
					case NOTYPE: 
					// printf("spaces\n");
					break;

					default: 
						tokens[nr_token].type = rules[i].token_type;
						tokens[nr_token].priority = rules[i].priority;
						strncpy(tokens[nr_token].str, substr_start, substr_len);
						tokens[nr_token].str[substr_len] = '\0';
						nr_token++;
				}

				break;
			}
		}

		if(i == NR_REGEX) {
			printf("no match at position %d\n%s\n%*.s^\n", position, e, position, "");
			return false;
		}
	}

	return true; 
}

bool check_parentheses(int l, int r){
	if(tokens[l].type == '(' && tokens[r].type == ')'){
		int lc = 0, rc = 0;
		int i;
		for(i = l + 1; i < r; i++){
			if(tokens[i].type == '('){
				lc++;
			}else if(tokens[i].type == ')'){
				rc++;
			}
			if(rc > lc){
				return false;
			}
		}
		if(lc == rc){
			return true;
		}
	}
	return false;
}

int dominant_operator(int l, int r){
	int i;
	int cnt = 0;
	int priority = 10;
	int op;
	for(i = l; i <= r; i++){
		if(tokens[i].type == DEC || tokens[i].type == HEX || tokens[i].type == REG){
			continue;
		}
		if(tokens[i].type == '('){
			cnt++;
		}
		if(tokens[i].type == ')'){
			cnt--;
		}
		if(cnt == 0 && tokens[i].priority <= priority){
			priority = tokens[i].priority;
			op = i;
		}
	}
	return op;
}

uint32_t eval(int p, int q){
	if(p > q){
		Assert(p>q, "something bad happeden\n");
		return 0;
	}else if(p == q){
		uint32_t ans;
		if(tokens[p].type == DEC){
			sscanf(tokens[p].str, "%d", &ans);
		}else if(tokens[p].type == HEX){
			sscanf(tokens[p].str, "%x", &ans);
		}else if(tokens[p].type == REG){
			int i;
			for(i = 0; i < 32; i++){
				if(!strcmp(tokens[p].str, regfile[i])){
					ans = cpu.gpr[i]._32;
				}
			}
			if(!strcmp(tokens[p].str, "$pc")){
				ans = cpu.pc;
			}
		}
		return ans;
	}else if(check_parentheses(p, q)){
		return eval(p + 1, q - 1);
	}else{
		int op = dominant_operator(p, q);
		// printf("%d\n", tokens[op].type );
		if(op == p || tokens[op].type == DEREF || tokens[op].type == MINUS || tokens[op].type == '!' ){
			// printf("here\n");
			uint32_t val = eval(op + 1, q);
			switch(tokens[op].type){
				case DEREF: 
					return mem_read(val, 4);
					break;
				case MINUS:
					return -val;
					break;
				case '!':
					return !val;
					break;
				default:
					Assert(1, "invalid operator\n");
			}
		}
		uint32_t val1 = eval(p, op - 1);
		uint32_t val2 = eval(op + 1, q);
		// printf("%d, %d\n", val1, val2);
		switch(tokens[op].type){
			case '+': return val1 + val2;
			case '-': return val1 - val2;
			case '*': return val1 * val2;
			case '/': return val1 / val2;
			case EQ:return val1 == val2;
			case NEQ:return val1 != val2;
			case AND:return val1 && val2;
			case OR:return val1 || val2;
			default:
			break;
		}
	}
	return 0;
}

uint32_t expr(char *e, bool *success) {
	if(!make_token(e)) {
		*success = false;
		return 0;
	}

	/* TODO: Insert codes to evaluate the expression. */
	int i;
	for(i = 0; i < nr_token; i++){
		if(tokens[i].type == '*' && (i == 0 || (tokens[i - 1].type != DEC && tokens[i - 1].type != HEX && tokens[i - 1].type != REG && tokens[i - 1].type != ')'))){
			tokens[i].type = DEREF;
			tokens[i].priority = 6;
		}
		if(tokens[i].type == '-' && (i == 0 || (tokens[i - 1].type != DEC && tokens[i - 1].type != HEX && tokens[i - 1].type != REG && tokens[i - 1].type != ')'))){
			tokens[i].type = MINUS;
			tokens[i].priority = 6;
		}
	}
	*success = true;
	uint32_t ans = eval(0, nr_token - 1);
	return ans;
}

