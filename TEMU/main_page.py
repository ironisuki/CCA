from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import os
import subprocess
import re


class MainPage(object):
    def __init__(self, master=None):
        self.testcase_list = []  # 存放测试样例名称
        self.temu_cmd_list = ['help']  # 需要执行的cmd
        self.testcase_name = StringVar()  # 正在测试的程序名
        self.cmd = StringVar()  # 正在执行的cmd
        self.end_flag = True  # 是否结束测试的flag

        # 先把gui_log.txt置为空
        with open("./gui_log.txt", "w+",encoding='utf-8') as f:
            f.write("\n")

        # 创建界面
        self.root = master
        winWidth = 650
        winHeight = 400
        screenWidth = self.root.winfo_screenwidth()
        screenHeight = self.root.winfo_screenheight()
        x = int((screenWidth - winWidth) / 2)
        y = int((screenHeight - winHeight) / 2)
        # 设置窗口初始位置在屏幕居中
        self.root.geometry("%sx%s+%s+%s" % (winWidth, winHeight, x, y))
        # 设置窗口图标
        # self.root.iconbitmap("./1.ico")
        # 设置窗口宽高固定
        self.root.resizable(0, 0)
        self.createPage()

    def createPage(self):
        self.page = Frame(self.root)  # 创建Frame
        self.page.pack()

        # get testcase列表
        try:
            retcode = os.popen('cd mips_sc/src && ls').read().split("\n")
            for item in retcode:
                if item[-2:] == '.S':
                    self.testcase_list.append(item)
        except Exception as err:
            print(err)

        # 选择测试程序
        Label(self.page, text='选择测试程序: ').grid(row=1, stick=W, pady=10)
        self.testcase_name.set('logic.S')
        self.combobox = ttk.Combobox(
            master=self.page,  # 父容器
            height=3,  # 高度,下拉显示的条目数量
            width=12,  # 宽度
            state='normal',  # 设置状态 normal(可选可输入)、readonly(只可选)、 disabled
            cursor='arrow',  # 鼠标移动时样式 arrow, circle, cross, plus...
            font=('', 12),  # 字体
            textvariable=self.testcase_name,  # 通过StringVar设置可改变的值
            values=self.testcase_list,  # 设置下拉框的选项
        )
        self.combobox.grid(row=1, column=1, stick=W)
        Button(self.page,
               text='确定',
               command=self.write_testcase_name,
               bg='AliceBlue').grid(row=1, column=5)

        # 输入测试指令
        self.cmd.set('help')
        Label(self.page, text='输入命令: ').grid(row=2, stick=W, pady=10)
        Entry(self.page, textvariable=self.cmd).grid(row=2, column=1, stick=W)
        Button(self.page, text='执行', command=self.show_info,
               bg='AliceBlue').grid(row=2, column=5)

        # 输出info
        self.sb = Scrollbar(self.root)
        self.sb.pack(side="right", fill="y")
        self.list = Listbox(self.root)
        self.list.config(yscrollcommand=self.sb.set)
        self.list.pack(fill=BOTH, expand=1, padx=10, pady=10)
        # self.list.pack(side="left", fill="both")
        # self.sb.config(command=self.list.yview)

    def write_testcase_name(self):

        self.end_flag = True  # 是否结束测试的flag
        self.temu_cmd_list = ['help']  # 需要执行的cmd
        # 把gui_log.txt置为空
        with open("./gui_log.txt", "w+",encoding='utf-8') as f:
            f.write("\n")

        with open("./mips_sc/src/makefile.testcase", "w+",encoding='utf-8') as f:
            # 不太懂为啥makefile.testcase这样写
            f.write(
                '# change the tesctcase name, 逆天bug,不能把user_program写到第一行\n')
            f.write('USER_PROGRAM := %s' %
                    self.testcase_name.get().split(".S")[0])
        message = '正在测试程序' + self.testcase_name.get()
        messagebox.showinfo(title='info', message=message)
        """启动终端"""
        # 清空list内容 ugly but useful
        self.list.destroy()
        self.list = Listbox(self.root)
        self.list.pack(fill=BOTH, expand=1, padx=10, pady=10)

        try:
            string = "--------------------------------正在测试程序：" + self.testcase_name.get() + "--------------------------------\n"
            self.list.insert("end", string.strip('\n'))
            self.sb.config(command=self.list.yview, width = 12,highlightcolor="AliceBlue")

        except Exception as err:
            print(err)

        self.write_log()

        with open("./gui_log.txt", "r",encoding='utf-8') as f:
            output_list = f.readlines() 
        for item in output_list:
            self.list.insert("end", item.strip('\n') )
            self.sb.config(command=self.list.yview, width = 12,highlightcolor="AliceBlue")


    def show_info(self):
        if not self.end_flag:
            messagebox.showinfo(title='warn', message='程序已经结束，请您更换程序进行测试')
            # 先把gui_log.txt置为空
            with open("./gui_log.txt", "w+",encoding='utf-8') as f:
                f.write("\n")
            return

        # 清空list内容 ugly but useful
        self.list.destroy()
        self.list = Listbox(self.root)
        self.list.pack(fill=BOTH, expand=1, padx=10, pady=10)

        try:
            string = "--------------------------------正在测试：" + self.testcase_name.get() + "--------------------------------\n"
            self.list.insert("end", string.strip('\n'))
            self.sb.config(command=self.list.yview, width = 12,highlightcolor="AliceBlue")

        except Exception as err:
            print(err)

        self.temu_cmd_list.append(self.cmd.get())
        self.write_log()

        with open("./gui_log.txt", "r",encoding='utf-8') as f:
            output_list = f.readlines()
        for item in output_list:
            self.list.insert("end", item.strip('\n'))
            self.sb.config(command=self.list.yview, width = 12,highlightcolor="AliceBlue")



    def write_log(self):
        with open("./gui_log.txt", "a+",encoding='utf-8') as f:
            temu_output = self.run_cmd_list()
            temp = temu_output.decode('utf-8').split("(temu) ")
            item  = temp[-2]
            
            if self.end_flag:
                if '\33[1;31mtemu: HIT GOOD TRAP\33' in item:
                    self.end_flag = False
                    # rule = r' $pc = (.*?)\n'
                    # slotList = re.findall(rule, item)
                    # 忘了re了，$PC 值不太好提取，只打个hit吧
                    f.write("\ntemu: HIT GOOD TRAP\n")
                    # print("\ntemu: HIT GOOD TRAP\n")
                    return
                item_list = item.split('\n')
                # print(len(item_list))
                # print(item_list)
                for i in range(len(item_list)):
                    if i == 0:
                        f.write("执行调试命令： " + item_list[i]+"\n")
                    else:
                        f.write(item_list[i]+"\n")

    def run_cmd_list(self):
        temu_terminal = subprocess.Popen(["make", "run"],
                                         stdin=subprocess.PIPE,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE)
        for temu_cmd in self.temu_cmd_list:
            string = temu_cmd + "\n"
            byte = string.encode()
            temu_terminal.stdin.write(byte)

        # 只有stdin关了才能get stdout
        temu_terminal.stdin.close()

        temu_output = temu_terminal.stdout.read()
        temu_terminal.stdout.close()

        return temu_output